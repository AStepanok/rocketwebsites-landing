window.sr = ScrollReveal();

function checkVisible(elm) {
    var rect = elm.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}


$(window).on('scroll', function () {

   var numbers = $('.monthlyPaid .odometer');

   for (var i = 0; i< numbers.length; i++) {

       if (checkVisible(numbers[i])) {

           $(numbers[i]).text($(numbers[i]).attr('data-number'));
       }
   }
});

$(document).ready(function() {
            $('.portfolio .slick').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                speed: 600,
                autoplay: false,
                focusOnSelect: false,
                prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            arrows: false,


                        }
                    },

                ]
                
            });

            $('.reviews .slick').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                speed: 600,
                autoplay: false,
                focusOnSelect: false,
                prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            arrows: false,


                        }
                    },

                ]
            });
});




$('.contactForm').submit(function() {
    var data = {};
// 
    
    
//    var data = getFormData($(this));
       data.emailData = getFormData($(this));
//    data._id = '5b27f4cc7f8fb85c503203bb';
    var d = new Date();
    data._id = 'mQgBiANyFGdhOSWiIVw4';
    
    var da =  d.getDate();
    var mo =  d.getMonth() + 1;
    var y =  d.getFullYear();
    var h =  d.getHours();
    var mi =  d.getMinutes();
    
    if (da < 10)
        da = '0' + da;
    if (mo < 10)
        mo = '0' + mo;
    if (h < 10)
        h = '0' + h;
    if (mi < 10)
        mi= '0' + mi;
    
    data.initialDate = da + '.' + mo + '.' + y + '  ' + h + ':' + mi;
     
    
//    
//    $.post({
//        url: 'https://crm.rocketwebsites.ru/requests',
//        data: data,
//        success: function (res) {
//            $('#successModal').modal('show');
//
//        },
//        error: function (res) {
//            $('#errorModal').modal('show');
//
//        }
//    });
    
    $.post({
        url: 'https://crm.rocketwebsites.ru/email',
        data: data,
        success: function (res) {
            var resObj = JSON.parse(res);
            console.log(resObj.status);
            
            if (resObj.status == 200)
                $('#successModal').modal('show');
            else
                $('#errorModal').modal('show');
        },
        error: function (res) {
            $('#errorModal').modal('show');
           console.log(res);

        }
    });
    
    return false;
    
})

function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

sr.reveal('.sr-1', {duration: 300, delay: 300});
sr.reveal('.sr-2',  {duration: 300, delay: 300});
sr.reveal('.sr-3', {duration: 300, delay: 300});
sr.reveal('.sr-4',  {duration: 300, delay: 300});
sr.reveal('.sr-5', {duration: 300, delay: 300});
sr.reveal('.sr-6',  {duration: 300, delay: 300});


// Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 50
                }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });